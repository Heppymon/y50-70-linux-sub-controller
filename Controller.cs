using System;

namespace y50_70_linux_sub_controller
{
    public class Controller
    {
        public Controller () { IsRun = true; }
        public bool IsRun { get; set; }
        public void DoWork()
        {
            while(IsRun){
                Console.WriteLine(DateTime.Now.ToShortDateString());
            }

        }

    }
}