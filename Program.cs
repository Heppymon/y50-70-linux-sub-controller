﻿using System;

namespace y50_70_linux_sub_controller
{
    public class Program
    {
        static void Main(string[] args)
        {
            try{
                var controller = new Controller();
                controller.DoWork();
                Console.ReadKey();
            }
            catch{
                Console.WriteLine("The error was occured!");
            }
        }
    }
}
